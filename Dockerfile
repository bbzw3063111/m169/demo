FROM alpine:3.17.2
RUN apk update
RUN apk add curl 
ENTRYPOINT ["curl", "ifconfig.me", "-q"]
